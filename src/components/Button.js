

function Button(props) {
    return (
        <button className={`${props.classNamePrefix ?? ''}`}>{props.value}</button>
    );
}

export default Button;
