import './App.css';
import Button from "./components/Button";
import {useState} from "react";

function App() {
  const [history, setHistory] = useState([]);
  const [currentValue, setCurrentValue] = useState('');

  return (
      <>
        <p>{history.at(-1)}</p>
        <span>C</span>
        <div className={'container'}>
          <Button value={7} />
          <Button value={8} />
          <Button value={9} />
          <Button value={4} />
          <Button value={5} />
          <Button value={6} />
          <Button value={1} />
          <Button value={2} />
          <Button value={3} />
          <Button classNamePrefix={'double'} value={0} />
          <Button value={'+'} />
          <Button value={'-'} />
          <Button value={'*'} />
          <Button value={'/'} />
          <Button value={'='} />
        </div>
      </>
  );
}

export default App;
